import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import Loading from './src/components/Loading';

import Game from './src/components/Game';

export default class App extends Component{
  state = {
    empresas: {},
    isReady: false
  };

  componentDidMount() {
    this.loadEmpresas();
  }

  async loadEmpresas() {

    try {
      const response = await fetch("http://empresas.ioasys.com.br/v1");
      const data = await response.json();
      this.setState({ empresas: data, isReady: true });
    } catch(error){
      console.log(error);
      }
  }

  render() {
    if (!this.state.isReady) {
      return <Loading />;
    }

    const { empresas } = this.state;

    return (
      <View styles={styles.styles}>
        {empresas.map((item, i) => <Game item={item} key={i} /> )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "flex-start",
    marginTop: 50,
    marginLeft: 10,
    marginRight: 10
  },
}) 